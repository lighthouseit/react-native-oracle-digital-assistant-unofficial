import { NativeModules, DeviceEventEmitter } from 'react-native';

type Message = {
  actions: string[];
  createdDate: number;
  footerText: string;
  headerText: string;
  isRead: boolean
}

type ChatListeners = {
  onMessage?: (message: Message) => void;
  onStatusChange?: (status: string) => void;
  onSpeechStopRecording?: () => void;
  onSpeechStartRecording?: () => void;
  onSpeechPartialResult?: (message: String) => void;
  onSpeechError?: (error: String) => void;
  onSpeechSuccess?: (message: String) => void;
}

type OracleDigitalAssistantTypes = {
  init: (userId: string, channelId: string, chatServer: string,) => Promise<void>;
  isInitialized: () => Promise<Boolean>;
  startRecording: () => Promise<void>;
  stopRecording: (force?: Boolean) => Promise<Boolean>;
  setSpeechLocale: (locale: string) => Promise<Boolean>;
  setupChatListeners: (chatListeners: ChatListeners) => Promise<void>;
  sendMessage: (message: string,) => Promise<void>;
};

const { OracleDigitalAssistant: OracleDigitalAssistantModule } = NativeModules;

const OracleDigitalAssistant = {
  ...OracleDigitalAssistantModule,
  setupChatListeners: (chatListeners: ChatListeners = {
    onMessage: () => { },
    onStatusChange: () => { },
    onSpeechStopRecording: () => { },
    onSpeechStartRecording: () => { },
    onSpeechPartialResult: () => { },
    onSpeechError: () => { },
    onSpeechSuccess: () => { },
  }) => {
    OracleDigitalAssistantModule.setupChatListeners();

    DeviceEventEmitter.addListener('onStatusChange', status => {
      if (chatListeners.onStatusChange)
        chatListeners.onStatusChange(status);
    });

    DeviceEventEmitter.addListener('onMessage', message => {
      if (chatListeners.onMessage)
        chatListeners.onMessage(JSON.parse(message.message));
    });

    DeviceEventEmitter.addListener('onSpeechStopRecording', () => {
      if (chatListeners.onSpeechStopRecording)
        chatListeners.onSpeechStopRecording();
    });

    DeviceEventEmitter.addListener('onSpeechStartRecording', () => {
      if (chatListeners.onSpeechStartRecording)
        chatListeners.onSpeechStartRecording();
    });

    DeviceEventEmitter.addListener('onSpeechPartialResult', message => {
      if (chatListeners.onSpeechPartialResult)
        chatListeners.onSpeechPartialResult(message.message);
    });

    DeviceEventEmitter.addListener('onSpeechError', message => {
      if (chatListeners.onSpeechError)
        chatListeners.onSpeechError(message.error);
    });

    DeviceEventEmitter.addListener('onSpeechSuccess', message => {
      if (chatListeners.onSpeechSuccess)
        chatListeners.onSpeechSuccess(message.message);
    });
  },
};

export default OracleDigitalAssistant as OracleDigitalAssistantTypes;
