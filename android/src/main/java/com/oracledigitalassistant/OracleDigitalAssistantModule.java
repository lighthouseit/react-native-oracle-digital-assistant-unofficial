package com.oracledigitalassistant;

import android.Manifest;
import android.content.pm.PackageManager;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import oracle.cloud.bots.mobile.core.Bots;
import oracle.cloud.bots.mobile.core.BotsCallback;
import oracle.cloud.bots.mobile.core.BotsConfiguration;
import oracle.cloud.bots.mobile.core.BotsSDKException;
/*import java.util.ArrayList;
import java.util.Locale;
import oracle.cloud.bots.mobile.core.SpeechSynthesisSetting;
*/

public class OracleDigitalAssistantModule extends ReactContextBaseJavaModule {

    OracleDigitalAssistantModule(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "OracleDigitalAssistant";
    }

    @ReactMethod
    public void init(String userId, String channelId, String chatServer, Promise promise) {
        try {
            // ArrayList<SpeechSynthesisSetting> speechSynthesisSettings = new ArrayList<>();
            // speechSynthesisSettings.add(new SpeechSynthesisSetting(new Locale("pt", "BR")));

            BotsConfiguration botsConfiguration = new BotsConfiguration.BotsConfigurationBuilder(chatServer, false, this.getReactApplicationContext()) // Configuration to initialize the SDK
                    .channelId(channelId)
                    .userId(userId)
                    .enableSpeechRecognition(true)
                    .enableSpeechRecognitionAutoSend(false)
                    // .enableSpeechSynthesis(true)
                    // .speechSynthesisVoicePreferences(speechSynthesisSettings)
                    .build();

            Bots.init(this.getCurrentActivity().getApplication(), botsConfiguration, new BotsCallback() {  // Initialize the SDK
                @Override
                public void onSuccess(Response paramResponse) {
                    // Bots.initSpeechSynthesisService();
                    // Bots.setSpeechLocale("pt-br");
                    promise.resolve(true);
                }

                @Override
                public void onFailure(Response paramResponse) {
                    promise.reject(new Exception(paramResponse.getErrorMessage()));
                }
            });
        } catch (BotsSDKException e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void isInitialized(Promise promise) {
        try {
            promise.resolve(Bots.isInitialized());
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void setupChatListeners() {
        Bots.setEventListener(new BotsEventListener(new BotsCallbacks(this.getReactApplicationContext())));
    }

    @ReactMethod
    public void sendMessage(String message, Promise promise) {
        try {
            Bots.sendMessage(message);

            promise.resolve(true);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void startRecording(Promise promise) {
        try {
            if (ContextCompat.checkSelfPermission(this.getCurrentActivity(),
                    Manifest.permission.RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this.getCurrentActivity(),
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        1234);
                promise.resolve(false);
            } else {
                Bots.startRecording(new BotsSpeechEventListener(new BotsSpeechCallbacks(this.getReactApplicationContext())));
                promise.resolve(true);
            }

//            if (ContextCompat.checkSelfPermission(this.getCurrentActivity(),
//                    Manifest.permission.CALL_PHONE)
//                    != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(this.getCurrentActivity(),
//                        new String[]{Manifest.permission.CALL_PHONE},
//                        1234);
//            }
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void stopRecording(Boolean force, Promise promise) {
        try {
            Bots.stopRecording(force);
            promise.resolve(true);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void setSpeechLocale(String locale, Promise promise) {
        try {
            Bots.setSpeechLocale(locale);
            promise.resolve(true);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    private void sendReactEvent(String eventName,
                                @Nullable WritableMap params) {
        this.getReactApplicationContext()
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
}
