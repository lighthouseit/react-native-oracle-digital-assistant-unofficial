package com.oracledigitalassistant;

public interface IBotsSpeechCallbacks {
    void onSuccess(String message);
    void onError(String error);
    void onPartialResult(String message);
    void onStartRecording();
    void onStopRecording();
}
