package com.oracledigitalassistant;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class BotsSpeechCallbacks implements IBotsSpeechCallbacks{
    public ReactApplicationContext reactContext = null;

    BotsSpeechCallbacks(ReactApplicationContext context){
        this.reactContext = context;
    }

    @Override
    public void onSuccess(String message) {
        WritableMap payload = Arguments.createMap();
        payload.putString("message", message);

        this.reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("onSpeechSuccess", payload);
    }

    @Override
    public void onError(String error) {
        WritableMap payload = Arguments.createMap();
        payload.putString("error", error);

        this.reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("onSpeechError", payload);
    }

    @Override
    public void onPartialResult(String message) {
        WritableMap payload = Arguments.createMap();
        payload.putString("message", message);

        this.reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("onSpeechPartialResult", payload);
    }

    @Override
    public void onStartRecording() {
        this.reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("onSpeechStartRecording", null);
    }

    @Override
    public void onStopRecording() {
        this.reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit("onSpeechStopRecording", null);
    }
}
