package com.oracledigitalassistant;

import oracle.cloud.bots.mobile.core.BotsSpeechResult;
import oracle.cloud.bots.mobile.core.IBotsSpeechListener;

public class BotsSpeechEventListener implements IBotsSpeechListener {
    BotsSpeechCallbacks botsCallbacks;

    BotsSpeechEventListener(BotsSpeechCallbacks botsCallbacks) {
        this.botsCallbacks = botsCallbacks;
    }

    @Override
    public void onActiveSpeechUpdate(byte[] bytes) {}

    @Override
    public void onError(String s) {
        this.botsCallbacks.onError(s);
    }

    @Override
    public void onSuccess(String s) {
        this.botsCallbacks.onSuccess(s);
    }

    @Override
    public void onSuccess(BotsSpeechResult botsSpeechResult) {}

    @Override
    public void onPartialResult(String s) {
        this.botsCallbacks.onPartialResult(s);
    }

    @Override
    public void onClose(int i, String s) {
        this.botsCallbacks.onStopRecording();
    }

    @Override
    public void onOpen() {
        this.botsCallbacks.onStartRecording();
    }
}
