package com.oracledigitalassistant;

public interface IBotsCallbacks {
    void onMessage(MessageDTO message);
    void onStatusChange(String status);
}
